export const ensureStartingSlash = (path: string) => {
    return path[0] == '/' ? path : `/${path}`;
};