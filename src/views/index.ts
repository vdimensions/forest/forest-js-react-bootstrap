import React from "react";
import BreadcrumbsMenu from './BreadcrumbsMenu';
import BreadcrumbsMenuItem from './BreadcrumbsMenuItem';
import BreadcrumbsMenuNavigableItem from './BreadcrumbsMenuNavigableItem';
import DialogSystem from "./DialogSystem";
import NavigationMenu from "./NavigationMenu";
import NavigationMenuItem from "./NavigationMenuItem";
import NavigationMenuNavigableItem from "./NavigationMenuNavigableItem";

let views : React.FC<any>[] = [
    DialogSystem,
    NavigationMenu, NavigationMenuItem, NavigationMenuNavigableItem,
    BreadcrumbsMenu, BreadcrumbsMenuItem, BreadcrumbsMenuNavigableItem
];

export default views;