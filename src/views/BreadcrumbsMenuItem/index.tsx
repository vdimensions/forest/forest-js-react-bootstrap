import React from 'react';
import { Breadcrumb } from 'react-bootstrap';
import { ForestView } from '@vdimensions/forest-js-react';
import { NavigationNode } from '@vdimensions/forest-js-ui/Navigation';

export default ForestView("BreadcrumbsMenuItem", (navItem: NavigationNode) => {
    let text = navItem.title || navItem.path;
    return ( 
        <Breadcrumb.Item active={navItem.selected}>
            {text}
        </Breadcrumb.Item>
    );
});
