import React, {Dispatch, useState} from 'react';
import {InputGroup, FormControl} from 'react-bootstrap';
import {FaClock} from "react-icons/fa";
import './index.scss';
import {createDate} from "../../utils/date-utils";

type TimePickerMandatoryProps = {
    id: string,
    name: string,
    size?: 'sm' | 'lg';
}
type TimePickerOptionalProps = {
    className: string
    value: Date
    onChange: Dispatch<any>;
}
export type TimePickerProps = TimePickerMandatoryProps & Partial<TimePickerOptionalProps>

const TimePicker = (props: TimePickerProps) => {
    const { id, name } = props;
    let [selectedTime, setSelectedTime] = useState<Date>(props.value || new Date());
    return (
        <InputGroup className={[props.className, "time-picker"].join(' ')} size={props.size}>
            <InputGroup.Text id={id +"-time-addon"}>
                <FaClock/>
            </InputGroup.Text>
            <FormControl
                type="number"
                size={props.size}
                aria-label="Hour"
                aria-describedby={id +"-time-addon"}
                value={selectedTime.getHours().toString()}
                onChange={(e:any) => {
                    let hours = e.target.value as number;
                    let minutes = selectedTime.getMinutes();
                    let seconds = 0;
                    let value = createDate(
                        selectedTime.getFullYear(),
                        selectedTime.getMonth(),
                        selectedTime.getDay(),
                        hours,
                        minutes,
                        seconds);
                    setSelectedTime(value);
                    props.onChange && props.onChange({target: {name: name, value: value}});
                }}
                />
            <InputGroup.Text id={id +"-colon-addon"}>:</InputGroup.Text>
            <FormControl
                type="number"
                size={props.size}
                aria-label="Minute"
                aria-describedby={id +"-colon-addon"}
                value={selectedTime.getMinutes().toString()}
                onChange={(e:any) => {
                    let hours = selectedTime.getHours();
                    let minutes = e.target.value as number;
                    let seconds = 0;
                    if (minutes > 60) {
                        hours += (minutes/60);
                        minutes = minutes % 60;
                    } else if (minutes < 0) {
                        hours += (minutes/60);
                        minutes = 60 - (Math.abs(minutes) % 60);
                    }
                    let value = createDate(
                        selectedTime.getFullYear(),
                        selectedTime.getMonth(),
                        selectedTime.getDay(),
                        hours,
                        minutes,
                        seconds);
                    setSelectedTime(value);
                    props.onChange && props.onChange({target: {name: name, value: value}});
                }}
                />
        </InputGroup>
    );

};

export default TimePicker;
