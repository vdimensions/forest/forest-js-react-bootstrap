import React, {Dispatch, useState} from 'react';
import {Col, Row, Button, Card, InputGroup, FormControl} from 'react-bootstrap';
import {FaAngleLeft, FaAngleRight} from "react-icons/fa";
import '../seven_col_grid.scss';
import './index.scss';
import {createDate, getMonthName} from "../../utils/date-utils";

export enum DaysOfWeek {
    Sunday,
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
}

type CalendarMandatoryProps = {
    size?: 'sm' | 'lg';
}

type CalendarOptionalProps = {
    firstDayOfWeek: DaysOfWeek;
    className: string,
    value: Date,
    onDateChanged: Dispatch<Date|undefined>;
    minDate: Date;
    maxDate: Date;
}

export type CalendarOptions = CalendarMandatoryProps & Partial<CalendarOptionalProps>;

const getWeekDayNames = (dates: Date[]) => {
    return dates.slice(7, 14).map(f => f.toLocaleString(window.navigator.language, {weekday: 'short'}));
};

const getDays = (date: Date, firstDayOfWeek: DaysOfWeek) => {
    let result: Date[] = [], 
        lastMonth = new Date(date.getFullYear(), date.getMonth(), 0),
        d = createDate(date.getFullYear(), date.getMonth(), 1);
    
    let numberOfPreviousMonthDaysInFirstWeek = d.getDay() - (firstDayOfWeek);
    if (numberOfPreviousMonthDaysInFirstWeek < 0) {
        numberOfPreviousMonthDaysInFirstWeek += 7;
    }

    for (let i = numberOfPreviousMonthDaysInFirstWeek - 1; i >= 0; i--) {
        result[result.length] = createDate(lastMonth.getFullYear(), lastMonth.getMonth(), (lastMonth.getDate() - i));
    }
    
    while (d.getMonth() === date.getMonth()) {
        result[result.length] = d;
        d = createDate(date.getFullYear(), date.getMonth(), (d.getDate() + 1));
    }

    d = createDate(date.getFullYear(), (date.getMonth() + 1), 1);
    let numberOfNextMonthDaysInLastWeek = 7 - (d.getDay() - (firstDayOfWeek));
    if (numberOfNextMonthDaysInLastWeek < 0) {
        numberOfNextMonthDaysInLastWeek += 7;
    }

    for (let k = 0; k < numberOfNextMonthDaysInLastWeek && numberOfNextMonthDaysInLastWeek !== 7; k++) {
        result[result.length] = d;
        d = createDate(date.getFullYear(), (date.getMonth() + 1), (d.getDate() + 1));
    }
    
    return result;
};

const changeMonth = (date: Date, m: number, add: boolean) => {
    let yearIncrement = date.getFullYear(), monthIncrement = date.getMonth() + m*(add ? 1 : -1);
    if (monthIncrement >= 12) {
        monthIncrement -= 12;
        yearIncrement +=1;
    } else if (monthIncrement < 0) {
        monthIncrement += 12;
        yearIncrement -=1;
    }
    return createDate(
        yearIncrement, monthIncrement, date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds());
};
const toggleMonth = (add: boolean, opts: CalendarOptions, state: CalendarState) => {
    return createCalendarState(opts, state, {baseDate: changeMonth(state.baseDate, 1, add)});
};
const toggleYear = (add: boolean, opts: CalendarOptions, state: CalendarState) => {
    return createCalendarState(opts, state, {baseDate: changeMonth(state.baseDate, 12, add)});
};

const createCalendarState : (opts: CalendarOptions, state: CalendarState, data?: any) => CalendarState = (opts, state, data) => {
    return new CalendarState(opts, {
        baseDate: state.baseDate, 
        selectedDate: state.selectedDate, 
        firstDayOfWeek: state.firstDayOfWeek,
        minDate: state.minDate,
        maxDate: state.maxDate,
        ...data});
};

const handleDateSelect = (d: Date, opts: CalendarOptions, calendarState: CalendarState, setCalendarState: Dispatch<CalendarState>) => {
    let isDeselected = false;
    if (calendarState.selectedDate && d.toISOString() === calendarState.selectedDate.toISOString()) {
        setCalendarState(createCalendarState(opts, calendarState, { selectedDate: undefined}));
        isDeselected = true;
    } else {
        setCalendarState(createCalendarState(opts, calendarState, { baseDate: d, selectedDate: d}));
    }
    opts.onDateChanged && opts.onDateChanged(isDeselected ? undefined : d);
};

class CalendarState {
    readonly baseDate: Date = new Date();
    readonly visibleDates: Date[] = [];
    readonly monthName: string = "";
    readonly weekDays: string[] = [];
    readonly firstDayOfWeek: DaysOfWeek = DaysOfWeek.Sunday;
    readonly selectedDate?: Date = undefined;
    readonly minDate?: Date;
    readonly maxDate?: Date;

    constructor(opts: CalendarOptions, data: any ={
        baseDate: new Date(),
        selectedDate: undefined,
        firstDayOfWeek: DaysOfWeek.Sunday,
        minDate: undefined,
        maxDate: undefined,
    }) {
        this.baseDate = data.baseDate;
        this.selectedDate = data.selectedDate;
        this.firstDayOfWeek = data.firstDayOfWeek;
        let dates = getDays(data.baseDate, this.firstDayOfWeek);
        this.visibleDates = dates;
        this.weekDays = getWeekDayNames(dates);
        this.monthName = getMonthName(data.baseDate, (opts.size && opts.size !== "sm") ? 'long' : 'short');
        this.minDate = data.minDate;
        this.maxDate = data.maxDate;
    }
}

const stripTime = (date?: Date) => {
    return date && createDate(date.getFullYear(), date.getMonth(), date.getDate());
};

const Calendar = (props: CalendarOptions) => {
    let now = new Date();
    let dateRef = props.value || now;
    let [state, setState] = useState<CalendarState>(
        new CalendarState(props, {
            ...props,
            baseDate: stripTime(dateRef), 
            selectedDate: stripTime(props.value)
        }));
    function isOutOfRange(opts: CalendarOptions,  d: Date) {
        let {minDate, maxDate} = opts;
        let minViolated = minDate && (d < minDate);
        let maxViolated = maxDate && (d > maxDate);
        return minViolated || maxViolated;
    }
    return (
        <div className={"calendar"}>
            <Row>
                <Col>
                    <InputGroup className="mb-3" size={"sm"}>
                        <Button 
                            variant="secondary" 
                            size={"sm"} 
                            onClick={() => setState(toggleMonth(false, props, state)) }
                            >
                            <FaAngleLeft/>
                        </Button>
                        <FormControl
                            type="text"
                            readOnly={true} 
                            size={"sm"} 
                            value={(state.monthName)}
                            />
                        <Button 
                            variant="secondary" 
                            size={"sm"} 
                            onClick={() => setState(toggleMonth(true, props, state)) }
                            >
                            <FaAngleRight/>
                        </Button>
                    </InputGroup>
                </Col>

                <Col>
                    <InputGroup className="mb-3" size={"sm"}>
                        <Button 
                            variant="secondary" 
                            size={"sm"} 
                            onClick={() => setState(toggleYear(false, props, state)) }
                            >
                            <FaAngleLeft/>
                        </Button>
                        <FormControl
                            type="number"
                            readOnly={true} 
                            size={"sm"} 
                            value={(state.baseDate).getFullYear().toString()}
                            />
                        <Button 
                            variant="secondary" 
                            size={"sm"} 
                            onClick={() => setState(toggleYear(true, props, state)) }
                            >
                            <FaAngleRight/>
                        </Button>
                    </InputGroup>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Card className="calendar">
                        <Card.Body>
                            <Card.Title as="div">
                                <Row className="seven-cols weekday-names">
                                    {state.weekDays.map(d => (
                                        <Col className="col-sm-1 mt-1 mb-1 pl-1 pr-1" key={d}>
                                            <div>{d}</div>
                                        </Col>
                                    ))}
                                </Row>
                                <Row>
                                    <Col as="hr"/>
                                </Row>
                            </Card.Title>
                            <Card.Text as="div">
                                <Row className="seven-cols">
                                    {state.visibleDates.map(d => (
                                    <Col className="days col-sm-1 mt-1 mb-1 pl-1 pr-1" key={d.toISOString()}>
                                        {state.selectedDate && state.selectedDate.toISOString() === d.toISOString()
                                        ? <Button
                                            onClick={(e:any) => {e.preventDefault(); handleDateSelect(d, props, state, setState);}}
                                            disabled={state.baseDate.getMonth() !== d.getMonth() || isOutOfRange(props, d)}
                                            variant="primary"
                                            size="sm"
                                            >
                                            {d.getDate()}
                                          </Button>
                                        : <Button
                                            onClick={(e:any) => {e.preventDefault(); handleDateSelect(d, props, state, setState);}}
                                            disabled={state.baseDate.getMonth() !== d.getMonth() || isOutOfRange(props, d)}
                                            variant="outline-secondary"
                                            size="sm"
                                            >
                                            {d.getDate()}
                                          </Button>
                                        }
                                    </Col>
                                    ))}
                                </Row>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </div>
    );
};

export default Calendar;
