export class DialogModel {
    public title: string;

    constructor({
        title = ""
    } : any) {
        this.title = title;
    }
}