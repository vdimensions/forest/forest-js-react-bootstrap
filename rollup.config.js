import peerDepsExternal from "rollup-plugin-peer-deps-external";
import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import typescript from "rollup-plugin-typescript2";
import postcss from "rollup-plugin-postcss";
import copy from "rollup-plugin-copy";
import autoprefixer from 'autoprefixer';

import { embedLicense, prepareChunks } from "@vdimensions/rollup-js-helpers";

const workDir = __dirname;

export default {
  input: prepareChunks({ workDir: `${workDir}/src/`, paths : ["views/index.ts", "components/**/index.ts*" ] }),
  output: [
    {
      dir: "dist",
      format: "es",
      sourcemap: true
    }
  ],
  plugins: [
    peerDepsExternal(),
    resolve(),
    commonjs(),
    typescript({ 
      useTsconfigDeclarationDir: true, 
      rollupCommonJSResolveHack: false,
      clean: true
    }),
    postcss({
     plugins: [autoprefixer()],
     sourceMap: true,
     extract: false,
     modules: false,
     use: ['sass']
    }),
    copy({
      targets: [
        {
          src: "package.json",
          dest: "dist"
        }
      ]
    }),
    embedLicense({ workDir: workDir }),
  ]
};
